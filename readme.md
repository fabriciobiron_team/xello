# Xello Test

**Created by:** Fabricio Biron
**E-mail:** fabriciobiron@gmail.com
**Website:** http://fabriciobiron.me
**Test URL:** http://fabriciobiron.me/xello

----
### Wordpress Login
The wordpress login credentials are included on the email message.

----

### Registration Plugin
For the Registration form I created a registration plugin located at `/wp-content/plugins/registration`. 
1. **`registration-plugin.php`** is the plugin core, responsible for activating, deactivating, handles and hook calls;
2. **`registration-store.php`** is a controller file that handles form request validating data and, if ok, calls a method to save data;
3. **`registration-class.php`** is the registration model that connects with database through $wpdb object.



### Theme

##### Theme structure
I decided to create a link between CSS classes, SASS files and WordPress template part files. 

![Theme Structure](doc/theme_structure.png)


**For example:** 
Inside theme files, such as Index or Page, there is a `get_template_part()` that calls `jumbotron-section` template. The whole HTML is under `.jumbotron-section` CSS class and the SASS file is also named `_jumbotron-section`.


**Hacking** 
Natively, WordPress is not able to handle SVG files in Media Library, so I have added a filter to allow this ability and now the theme is able to receive SVG files.

In addition, I would like to mention I set up the HTML/CSS without any file reference such as Photoshop and Sketch. For that reason, I used Chrome extensions like rules and color picker to size each style property like margin/padding, width/height, even fonts I had to have a precise eye to fit into the layout as much as I could.

----
### Editable areas
You are able to change the content through editing pages. See example bellow: 


![Theme Structure](doc/editable_areas.gif)



----
### SEO
Post and Page types have the ability to add tags. Those tags not only have been showing in pages but also at meta html keywords.  


![SEO](doc/seo.gif)


As per requested, here follows a short description on _"how SEO content could be kept up to date with this choice of technology"_, which is WordPress:

- **Friendly URLs, keywords and/or short phrases, and hyphens to separate the words**: search engines can easily identify pages as relevant, and users can have an idea about the content and search match before clicking.
  > WordPress is able to customize those options in `settings > permalinks`.

- **Meta Tags**: include keywords in meta title and description, which will improve visibility, readability to users, and conversion rates (because the content will make sense to them).
  > As mentioned above, I printed page/post tags in `<meta name="keywords" />` and `<meta name="description" />` html tags.

- **Alt Attributes**: Important to check if all images alt attributes are descriptive in order to be W3C-compliant and inclusive to those who cannot see images.

- **Robots.txt**: Be aware to prevent some pages to be indexed by search engines, otherwise there might be duplicated, or divergent content out there, easily accessible to users. 

- **Accurate Page Title** (<title> tag): placed within the <head> element of the HTML document, it allows communicate a clear message to the search engines and users about the content.

- **Performance**: strive in delivering clean and light pages, by optimizing size of images/videos and avoiding clutter, which means a faster and better user experience. 

- **Mobile-friendly**: use the tools on WordPress to allow a responsive web design.

- **Fresh content**: Feed WordPress with content so it can come into fresh and consistent updates, increasing search indexes.

----

Finally, thank you for the oportunity to demonstrate some of my technical skills. 

Regards,

Fabricio Biron
