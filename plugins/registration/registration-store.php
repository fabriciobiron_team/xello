<?php

require( '../../../'.'/wp-blog-header.php' );
require_once( './registration-class.php' ); 

global $wpdb;
$isValid = true;
$message = '';

//first check
if( $_POST['fullname'] === "" || $_POST['email'] === "" ){
    $message =  [
        'type'=> 'warning',
        'text'=> 'Oops! The fields Name and E-mail are required!'
    ];
    set_session_flash('message',$message);
    header('Location: ' . $_SERVER['HTTP_REFERER'] );
    set_session_flash('fields',$_POST);
    exit;
}else{
    $isValid = true;
}

// second check
if($isValid){
    $registration = new Registration();
    $registration->fullname = esc_html($_POST['fullname']);
    $registration->email = (is_email($_POST['email'])) ? $_POST['email'] : $isValid = false;

    $registration->accepting_news = (isset($_POST['accepting_news']) == 'on') ? 1 : 0;
    
    if($isValid){
        $registration->save();
        $message =  [
            'type'=> 'success',
            'text'=> 'Hooray! You have been registered to receive our news!'
        ];
        header('Location: ' . $_SERVER['HTTP_REFERER'] );
        set_session_flash('message',$message);
    }else{
        $message =  [
            'type'=> 'warning',
            'text'=> 'Oops! E-mail is not valid!'
        ];        
        set_session_flash('fields',$_POST);
        set_session_flash('message',$message);
        header('Location: ' . $_SERVER['HTTP_REFERER'] );
    }

}