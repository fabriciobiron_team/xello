<?php

class Registration{

    public $fullname,$email,$wpdb;

    public function __construct(){
        global $wpdb;
        $this->wpdb = $wpdb;
        // $this->wpdb = $wpdb;
    }

    public function save(){
        // echo "entrou";exit;
        $data = [
            'fullname'          =>  $this->fullname,
            'email'             =>  $this->email,
            'accepting_news'             =>  $this->accepting_news,
        ];
        
        $this->wpdb->insert( 
            $this->wpdb->prefix.'registrations', $data        
        );

        return $this->wpdb->insert_id;

    }

    public function get_all(){
        $results = $this->wpdb->get_results( "SELECT * FROM {$this->wpdb->prefix}registrations", OBJECT );
        // var_dump($results);exit;
        return $results;
    }

}