<?php
/*
Plugin Name: Xello Registration
Plugin URI: 
Description: This is a plugin to register people. 
Author: Fabricio Biron 
Version: 1.0
Author URI: www.fabriciobiron.me
*/


/**
 * Load classes and dependencies if exits
 */
// spl_autoload_register(function ($class_name) {
//     $filename = dirname(__FILE__).'\\'.$class_name . '.php';
//     // echo $filename;
//     if (file_exists($filename)) {
//         include $filename;
//     }
// }); 

session_start();
require (__DIR__.'/registration-class.php');
define("plugin_dir", plugins_url());
 
/**
 * Plugin activation
 */
function xello_registration_activate() {

    // Creating database
    global $wpdb;

    $table_name = $wpdb->prefix . "registrations";

    $sql = "CREATE TABLE IF NOT EXISTS {$table_name} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fullname` VARCHAR(255) NULL,
  `email` VARCHAR(255) NULL,
  `accepting_news` TINYINT(1) NULL,
  `registration_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`))
        ENGINE = InnoDB";
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

register_activation_hook( __FILE__, 'xello_registration_activate' );
    
    
/**
 * Function to deactivate the plugin and
 * Add delete created page for the plugin
 * @return void
 */
function xello_registration_deactivate() {
    $post_id = get_option('xello_registration_plugin_page_id');
    wp_delete_post( $post_id);
    delete_option( 'xello_registration_plugin_page_id' );
}

register_deactivation_hook( __FILE__, 'xello_registration_deactivate' );
    


   
// ADMIN > OPTIONS
function xello_registration_menu() {
    add_menu_page( 'My xello_registration Plugin Options', 'Xello Registration', 'manage_options', 'xello_registration-plugin', 'xello_registration_options' );
}
add_action( 'admin_menu', 'xello_registration_menu' );


function xello_registration_options(){

    $registrations = new Registration();
    ?>

    <h1 class="wp-heading-inline">Registered </h1>
    <table class="wp-list-table widefat fixed striped posts">
        <tr>
            <th>Full Name</th>
            <th> E-mail </th>
            <th> Accepting News</th>
            <th>Registration Date</th>
        </tr>
        <?php foreach($registrations->get_all() as $row): ?>
        <tr>
            <td><?=$row->fullname;?></td>
            <td><?=$row->email;?></td>
            <td><?=$row->accepting_news;?></td>
            <td><?=$row->registration_date;?></td>
        </tr>
        <?php endforeach; ?>
    </table>
    
<?php

} //end of xello_registration_options



function register_form_handle(){
    
    ?>
    <?php $requested_values = get_session_flash('fields'); ?>
    <?php if(!is_null($msg = get_session_flash('message'))): ?>
        <div class="alert alert-<?=$msg['type'];?>" role="alert">
            <?=$msg['text'];?>
        </div>
    <?php endif; ?>
    <form action="<?php echo plugin_dir.'/registration/registration-store.php'  ?>" method="post">
    <div class="form-group">
        <label for="formGroupExampleInput">Name</label>
        <input maxlength="50" type="text" name="fullname" class="form-control form-control-lg" id="formGroupExampleInput" value="<?=$requested_values['fullname'] ;?>" placeholder="Type your name here">
    </div>
    <div class="form-group">
        <label for="formGroupExampleInput2">E-mail</label>
        <input maxlength="50" type="text" class="form-control form-control-lg" name="email" id="formGroupExampleInput2" placeholder="youremail@email.com"  value="<?=$requested_values['email'] ;?>">
    </div>
    <div class="form-group">
        <input  type="checkbox" name="accepting_news" aria-label="Checkbox for following text input" />
        <label class="form-check-label" for="exampleCheck1">Send me news by email</label>
    </div>
    <button type="submit" class="btn btn-primary">Register</button>
</form>

<?php
}




add_shortcode('register-form', 'register_form_handle');



function set_session_flash($name,$value){
    $_SESSION[$name] = $value;
}

function get_session_flash($name){
    // var_dump($_SESSION[$name]);
    $value =  $_SESSION[$name];
    unset($_SESSION[$name]);
    return $value;
}