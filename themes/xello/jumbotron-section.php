<!-- jumbotron section -->
<div class="jumbotron-section ">
    <div class="jumbotron">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-6 col-sm-12 featured-image-box">
                    <img class="featured-image img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/images/jumping-students.svg" />
                </div>  

                <div class="col-lg-6 col-sm-12 lettering">
                    <h5 class="hat">Lorem ipsum dolor</h5>
                    <h1 class="display-4 title">Lorem ipsum dolor sit amet, consectetur</h1>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- end of jumbotron sec --> 