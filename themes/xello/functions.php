<?php

// Turning on excerpt 
add_post_type_support( 'page', 'excerpt' );
add_theme_support( 'post-thumbnails' );

function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
  }
  add_filter('upload_mimes', 'cc_mime_types');

  function get_featured_svg(){
    echo wp_get_attachment_image_src(get_post_thumbnail_id( get_the_id()))[0];
  }



  // add tag support to pages
function tags_support_all() {
	register_taxonomy_for_object_type('post_tag', 'page');
}

add_action('init', 'tags_support_all');



function get_keywords(){
  global $post;
  $post_tags = get_the_tags();
 
  if ( $post_tags ) {
      foreach( $post_tags as $tag ) {
      echo $tag->name . ', '; 
      }
  }
}
