<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php bloginfo('name'); ?>  <?php wp_title(); ?></title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css" crossorigin="anonymous">
    <!-- <link href="https://use.fontawesome.com/releases/v5.0.7/css/all.css" rel="stylesheet"> -->
    <meta name="description" content="<?php if ( is_single() ) {
        single_post_title('', true); 
    } else {
        bloginfo('name'); echo " "; bloginfo('description');
    }
    ?>" />
    <meta name="keywords" content="<?=get_keywords()?>" />

  </head>

  <body>
    
    <div class="site">
 
        <div class="site-container ">

          <?php get_template_part( 'navigation-section' ); ?>
          <?php get_template_part( 'jumbotron-section' ); ?>