<?php get_header(); ?>
<?php
$tag = $wp_query->query['tag'];
$args = ['post_type'  =>  'page',"tag"=>$tag];
$wp = new WP_Query( $args ); 
// var_dump($wp);
?>

<!-- content-section-1 -->
<div class="content-section-1">
    <div class="container-fluid">
		<div class="row wrapper">
			<div>
				<h1 class="title">Posts by <?=$tag;?> name</h1><hr/>
				<div class="clearfix"></div>
       		 <?php if($wp->have_posts()): ?>
                <?php  while ( $wp->have_posts() ) : $wp->the_post();  ?>
           
              	  <?php get_template_part('content'); ?>                  

        
			<?php endwhile; endif; ?>
</div>
        </div>
    </div>
</div>
<!-- end of content-section-1 -->
<?php get_footer(); ?> 