<?php

$args = ['page_id'  =>  9];
$wp = new WP_Query( $args ); 

?>
<!-- content-section-3 -->
<div class="content-section-3">
    <div class="container-fluid">
        <div class="row wrapper">

            <div class="col-xl-6 col-sm-12 box">
                <?php if($wp->have_posts()): ?>
                <?php  while ( $wp->have_posts() ) : $wp->the_post();  ?>
                        <?php get_template_part('content-section-lettering'); ?>                  
                <?php endwhile; endif; ?>  
            </div>

            <div class="col-xl-6 col-sm-12 featured-image-box box">
                <img  class="img-fluid featured-image" src="<?php get_featured_svg() ?>" />
            </div>

        </div>
    </div>
</div>
<!-- end of content-section-3 -->