<!-- highlighted-quote -->
<div class="highlighted-quote-section">
    <div class="container-fluid">
        <div class="row wrapper">
            <div class="col">
                <div class="row logo">
                    <img class="featured-image " src="<?php echo get_template_directory_uri(); ?>/assets/images/xello-symbol-green-smile.svg" width="84"/>
                </div>
                <div class="row text-center">
                    <cite class="title">"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mollis ullamcorper"</cite>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end of content-section-3 -->