 <!-- social-footer -->
 <div class="social-footer-section">
    <div class="logo">
        <img class="logo-shadow" src="<?php echo get_template_directory_uri(); ?>/assets/images/xello-logo-white-shadow.svg" />
        <img class="logo-main" src="<?php echo get_template_directory_uri(); ?>/assets/images/xello-logo.svg" />
    </div> 
    <div class="social-list">
        <div class="social-list-item">
               
            <a href="https://www.youtube.com/user/CareerCruising2"  target="_blank">
                <img  src="<?php echo get_template_directory_uri(); ?>/assets/images/twitter.svg" />
            </a>
        </div>
        <div class="social-list-item">
            <a href="https://www.facebook.com/xellofuture/"   target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/facebook.svg" /></a>
        </div>


        <div class="social-list-item">
            <a href="https://www.linkedin.com/company/career-cruising"   target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/linkedin.svg" /></a>
        </div>

        <div class="social-list-item">
            <a href="https://twitter.com/xellofuture?lang=en"   target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/twitter.svg" /></a>
        </div>

    </div>
</div>
<!-- end of social-footer -->