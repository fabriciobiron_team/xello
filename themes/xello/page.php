<?php get_header(); ?>

	<div class="container-fluid page-section">
		<div class="row wrapper">
			<div class="col-md-6 col-sm-12 lettering box">
			<?php
				while ( have_posts() ) : the_post();
					get_template_part( 'content', get_post_format() );
					// If comments are open or we have at least one comment, load up the comment template.
					
					the_tags();
					
				endwhile;
				?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
