<!-- content-3columns -->
<div class="content-3columns-section">
    <div class="container-fluid">
        <div class="row wrapper">

            <div class="col col-lg-4 col-md-12 col-sm-12  box">
                <div class="featured-image-box">
                    <img class="image-1" src="<?php echo get_template_directory_uri(); ?>/assets/images/img-school.svg" />
                </div>
                <h3>Lorem ipsum dolor sit amet</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mollis ullamcorper urna sit amet suscipit. Cras sed turpis erat. 
                </p>
            </div>

            <div class="col col-lg-4 col-md-12 col-sm-12  box">
                <div class="featured-image-box">
                    <img  class="image-2" src="<?php echo get_template_directory_uri(); ?>/assets/images/img-rocket.svg" />
                </div>
                <h3>Lorem ipsum dolor sit amet</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mollis ullamcorper urna sit amet suscipit. Cras sed turpis erat. 
                </p>
            </div>

            <div class="col col-lg-4 col-md-12 col-sm-12  box">
                <div class="featured-image-box">
                    <img  class="image-3" src="<?php echo get_template_directory_uri(); ?>/assets/images/img-grad.svg" />
                </div>
                <h3>Lorem ipsum dolor sit amet</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mollis ullamcorper urna sit amet suscipit. Cras sed turpis erat. 
                </p>
            </div>

        </div>
    </div>
</div>
<!-- end of content-3columns -->