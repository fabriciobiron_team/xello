<?php

$args = ['page_id'  =>  11];
$wp = new WP_Query( $args ); 

?>
<!-- content-section-2 -->
<div class="content-section-2">
    <div class="container-fluid">
        <div class="row wrapper">
            <div class="col-xl-4 col-sm-12 featured-image-box box">
                <img class="img-fluid featured-image" src="<?php get_featured_svg() ?>"   />
            </div>
            
            <?php if($wp->have_posts()): ?>
            <?php  while ( $wp->have_posts() ) : $wp->the_post();  ?>
                <div class="col-xl-8 col-sm-12 lettering box">
                    <?php get_template_part('content-section-lettering'); ?>                  
                </div>
            <?php endwhile; endif; ?>
        </div>
    </div>
</div>
<!-- end of content-section-2 -->