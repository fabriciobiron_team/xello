<?php get_header(); ?>


<?php get_template_part( 'content-section-1' ); ?>
<?php get_template_part( 'content-section-2' ); ?>
<?php get_template_part( 'content-section-3' ); ?>
<?php get_template_part( 'highlighted-quote-section' ); ?>
<?php get_template_part( 'content-3columns-section' ); ?>


<?php get_footer(); ?>
