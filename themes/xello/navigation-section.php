<?php

$menuList = wp_get_nav_menu_items('Main'); 
?>

<!-- navbar -->
<div class="navigation-section ">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light">
  
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
                
            <a class="navbar-brand" href="<?=get_home_url(); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/xello-logo-white.svg" width="93" alt="">
            </a>
   
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <ul class="navbar-nav mt-2 mt-lg-0">

                    <?php foreach($menuList as $menu): ?>           


                    <li class="nav-item active">
                        <a class="nav-link" href="<?=$menu->url?>"><?=$menu->title;?> </a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#">Disabled</a>
                    </li> -->

                    <?php endforeach; ?>
                </ul>

                <div>
                    FR
                </div>
            </div>
        </nav>
    </div>
</div>
<!-- navbar -->