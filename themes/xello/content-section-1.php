<?php

$args = ['page_id'  =>  6];
$wp = new WP_Query( $args ); 

?>

<!-- content-section-1 -->
<div class="content-section-1">
    <div class="container-fluid">
        <div class="row wrapper">
        <?php if($wp->have_posts()): ?>
                <?php  while ( $wp->have_posts() ) : $wp->the_post();  ?>
           
            <div class="col-md-6 col-sm-12 lettering box">
                <?php get_template_part('content-section-lettering'); ?>                  
            </div>

            <div class="col-md-6 col-sm-12 form box">
            <?php echo do_shortcode("[register-form]"); ?>
            </div>
            <?php endwhile; endif; ?>
        </div>
    </div>
</div>
<!-- end of content-section-1 -->